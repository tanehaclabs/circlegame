package com.example.taneha.assignment6;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity implements View.OnTouchListener {
    TextView textview;
    RelativeLayout area;
    View circleId;
    String color = "Blue";
    int circle;
    int colorCode;
    Button nextButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textview = (TextView) findViewById(R.id.tv2);
        area = (RelativeLayout) findViewById(R.id.relative_layout);
        circleId = findViewById(R.id.circle_id);
        nextButton = (Button) findViewById(R.id.next_btn);
        area.setOnTouchListener(this);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SecondLevel.class);
                startActivity(intent);
            }
        });
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {

        float x = area.getWidth() / 2;
        float y = area.getHeight() / 2;
        float radius = circleId.getWidth() / 2;
        float xCoordinate = event.getX();
        float yCoordinate = event.getY();
        float distanceX = (float) Math.pow(x - xCoordinate, 2);
        float distanceY = (float) Math.pow(y - yCoordinate, 2);
        float result = (float) Math.sqrt(distanceX + distanceY);
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (radius > result) {
                    textview.setText("You Are Inside The Circle!");
                    if (color.equals("Blue")) {
                        circleId.setBackgroundResource(R.drawable.pinkcircle);
                        color = "Pink";
                    } else {
                        circleId.setBackgroundResource(R.drawable.circle);
                        color = "Blue";
                    }
                } else {
                    textview.setText("You Are Outside The Circle!");
                }
                return true;
            case MotionEvent.ACTION_MOVE:
                if (radius > result) {
                    circle = 1;
                    textview.setText("You Are Inside The Circle!");
                    if (color.equals("Blue")) {
                        circleId.setBackgroundResource(R.drawable.pinkcircle);
                        colorCode = 0;

                    } else {
                        circleId.setBackgroundResource(R.drawable.circle);
                        colorCode = 1;
                    }
                } else {
                    circle = 0;
                    textview.setText("You Are Outside The Circle!");
                    if (colorCode == 0)
                        color = "Pink";
                    else color = "Blue";
                }


        }
        return false;
    }
}