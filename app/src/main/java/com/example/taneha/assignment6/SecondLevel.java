package com.example.taneha.assignment6;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class SecondLevel extends ActionBarActivity {
    RelativeLayout touchLayout;
    ImageButton[] circleView = new ImageButton[6];
    TextView positionText;
    float[] xCoordinate = new float[6];
    float[] yCoordinate = new float[6];
    float[] distanceArray = new float[6];
    int count;
    String position;
    String[] color = new String[6];
    int previousCircle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_level);
        count = 0;
        touchLayout = (RelativeLayout) findViewById(R.id.relative_layout);
        positionText = (TextView) findViewById(R.id.tv_position);
        for (int i = 0; i < touchLayout.getChildCount(); i++) {
            if (touchLayout.getChildAt(i) instanceof ImageButton) {
                circleView[i] = (ImageButton) touchLayout.getChildAt(i);
            }
        }
        for (int i = 0; i < 6; i++) {
            color[i] = "Blue";
        }

        touchLayout.setOnTouchListener(new TouchClass());

    }


    class TouchClass implements View.OnTouchListener {

        @Override
        public boolean onTouch(View v, MotionEvent event) {

            float radius = circleView[1].getWidth() / 2;
            float x = event.getX();
            float y = event.getY();
            for (int i = 0; i < 6; i++) {
                xCoordinate[i] = circleView[i].getX() + radius;
                yCoordinate[i] = circleView[i].getY() + radius;
                distanceArray[i] = (float) Math.sqrt((Math.pow(x - xCoordinate[i], 2) + (Math.pow(y - yCoordinate[i], 2))));
            }
            for (int i = 0; i < 6; i++) {
                if (distanceArray[i] < radius) {
                    count = i;
                    position = "You are inside circle " + (i + 1)+ "!";
                    break;
                } else {
                    count = 7;
                    position = "You are outside the circle!";
                }
            }

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    positionText.setText(position);
                    if (count < 7) {
                        if (color[count].equals("Blue")) {
                            circleView[count].setBackgroundResource(R.drawable.pinkcircle);
                            color[count] = "Pink";
                        } else {
                            circleView[count].setBackgroundResource(R.drawable.circle);
                            color[count] = "Blue";
                        }
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    break;
                case MotionEvent.ACTION_MOVE:
                    if (count < 6 && count != previousCircle) {
                        if (color[count].equals("Blue")) {
                            circleView[count].setBackgroundResource(R.drawable.pinkcircle);
                            color[count] = "Pink";
                        } else {
                            circleView[count].setBackgroundResource(R.drawable.circle);
                            color[count] = "Blue";
                        }
                        previousCircle = count;
                    } else previousCircle = count;
                    break;
            }

            return true;
        }
    }


}